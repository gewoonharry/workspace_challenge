let userBalance;
const stock = [];
const order = [];
let total = 0;

$(function() {
    service.getUser()
        .then(function (user) {
            $('#user-name').text(user.login);
            $('#gold-balance').text(user.balance);
            userBalance = user.balance;
        })
        .then(service.list)
        .then(function(items) {
            const ul = $('#stock');
            items.forEach(function (item) {
                ul.append('<li>' + item.name + ': ' + item.quantity + '</li>');
                stock.push({
                    id: item.id,
                    quantity: item.quantity,
                });
            });
        });

    $('#buy-items').on('click', function() {
        $('.backdrop').removeClass('hidden');
        $('.backdrop').addClass('show');
        service.list()
            .then(function(items) {
                const modal = $('#order');
                const modalDiv = modal.append('<div class="order" id="order-box"></div').find('#order-box');
                    modalDiv.append('<div class="order__row order__row--header"><span class="order__title">Order</span><img src="icons/cross.png" class="order__icon order__icon--close" onclick="closeOrder()"></div>');
                    items.forEach(function(item) {
                        const orderRowDiv = modalDiv.append('<div class="order__row" id="row' + item.id + '"></div>').find('#row' + item.id);
                        orderRowDiv.append('<div class="order__left-box"><img src="images/'+ item.image + '" alt="'+ item.image + '" class="order__image"><span class="order__name">'+ item.name + '</span></div><div class="order__middle-box"><input type="image" src="icons/minus.png" class="order__icon" id="minus' + item.id + '" onclick="onDetractAmount(' + item.id + ', ' + item.price + ')" disabled><input type="text" placeholder="0" id="amount'+ item.id +'" class="order__input" disabled><input type="image" src="icons/plus.png" class="order__icon plus"  id="plus' + item.id + '" onclick="onAddAmount(' + item.id + ', ' + item.price + ')"></div><div class="order__right-box"><div class="order__gold">'+ item.price +' gold</div></div>');       
                        if (item.quantity === 0) {
                            $('#plus' + item.id).attr('disabled', true); // Disable plus button if out of stock
                            $('#plus' + item.id).attr('title', 'Out of stock'); // Display out of stock on hover
                        }
                    });
                    modalDiv.append('<div class="order__row order__row--footer"><span>Total</span><span id="message"></span><span id="total">0 gold</span></div><div class="order__row order__row--buttons"><div class="order__button"><button class="btn btn--blue" onClick="orderConfirm()">Buy</button><button class="btn" onclick="closeOrder()">Cancel</button></div></div>');
            })
    });
});

function onAddAmount(itemId, itemPrice) {
    let add = true;
    let message = '';
    checkBalance(itemPrice);
    if (positiveBalance) {
        checkStock(itemId);
        if (itemAvailable) {
            changeStock(add, itemId);
            changeOrder(add, itemId, itemPrice);
            changeTotal(add, itemPrice);
            changeInputs(add);
        }
    } else {
        message = 'Not enough gold';
    }
    $('#message').text(message);
    $('#total').text(total + ' gold');
}

function onDetractAmount(itemId, itemPrice) {
    let add = false;
    changeStock(add, itemId);
    changeOrder(add, itemId, itemPrice);
    changeTotal(add, itemPrice);
    changeInputs(add);
    $('#total').text(total + ' gold');
}

function checkBalance(itemPrice) {
    positiveBalance = true;
    if ((total + itemPrice) > userBalance) {
        positiveBalance = false;
    } 
    return positiveBalance;
}

function checkStock(itemId) {
    stock.forEach(function(item) {
        if (itemId === item.id) {
            if (item.quantity > 0 ) {
                itemAvailable = true;
            } else {
                itemAvailable = false;
            }
        }
    });
    return itemAvailable;
}

function changeStock(add, itemId) {
    stock.forEach(function(item) {
        if (itemId === item.id) {
            if (add) {
                item.quantity -= 1;
            } else if (!add) {
                item.quantity += 1;
            }
        }
    });
}

function changeOrder(add, itemId, itemPrice) {
    let itemIdExists = false;
    if (order.length) {
        order.forEach(function(item) {
            if (itemId === item.id) {
                itemIdExists = true;
                if (add) {
                    item.amount += 1;
                } else {
                    item.amount -= 1;
                }
            } 
        });
    }
    if (!order.length || !itemIdExists) {
        order.push({
            id: itemId,
            amount: 1,
            price: itemPrice
        });
    }
}

function changeTotal(add, itemPrice) {
    if (add) {
        total += itemPrice;
    } else {
        total -= itemPrice;
    } 
    return total;
}

function changeInputs(add) {
    order.forEach(function(item) {
        $('#amount' + item.id).val(item.amount);    // change visual amount
    });
    if (order.length) {
        order.forEach(function (item) {
            if (item.amount > 0) {
                $('#minus' + item.id).removeAttr('disabled');   // remove disable min button
            }
            if (!add) {
                if (item.amount < 1) {
                    $('#minus' + item.id).attr('disabled', true);   // add disable min button
                }
            }
        });
    }
    stock.forEach(function(item) {
        if (item.quantity < 1) {
            $('#plus' + item.id).attr('disabled', true);    // add disable plus button (out of stock)
            $('#plus' + item.id).attr('title', 'Out of stock'); // Display out of stock on hover
        }
        if (!add) {
            if (item.quantity > 0) {
                $('#plus' + item.id).removeAttr('disabled');   // remove disable plus button
            }
        }
    });
}

function orderConfirm() {
    updateStockBackend();
    updateUserBalance();
    closeAfterBuy();
}

function updateStockBackend() {
    const ul = $('#stock');
    $('#stock').empty();
    service.list()
        .then(function(items) {
            items.forEach(function(item) {
                stock.forEach(function (stockItem) {
                    if (item.id === stockItem.id) {
                        item.quantity = stockItem.quantity; 
                    }
                });
                ul.append('<li>' + item.name + ': ' + item.quantity + '</li>'); // update stock on home
            });
        });
}

function updateUserBalance() {
    userBalance -= total;
    service.getUser()
        .then(function (user) {
            user.balance = userBalance;
            $('#gold-balance').text(user.balance);
        });
}

function resetStock() {
    stock.length = 0;   // stock is a const
    service.list()  // fill with default values again
        .then(function(items) {
            items.forEach(function (item) {
                stock.push({
                    id: item.id,
                    quantity: item.quantity,
                });
            });
        });
}

function closeAfterBuy() {
    total = 0;
    order.length = 0; // order is a const
    $('#order').empty();
    $('.backdrop').addClass('hidden');
}

function closeOrder() {
    resetStock();
    total = 0;
    order.length = 0; // order is a const
    $('#order').empty();
    $('.backdrop').addClass('hidden');
}