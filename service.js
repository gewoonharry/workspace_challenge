const service = (function() {
    const user = {
        id : 1,
        login : "Harry",
        balance : 120
    };

    const items = [
        {
            id : 1,
            name : "Bronze sword: low quality, low price",
            price : 8,
            quantity : 10,
            image: "bronze_sword.png"
        },
        {
            id : 2,
            name : "Wooden shield",
            price : 15,
            quantity : 5,
            image: "wooden_shield.png"
        },
        {
            id : 3,
            name : "Battle axe",
            price : 12,
            quantity : 2,
            image: "battle_axe.png"
        },
        {
            id : 4,
            name : "Longsword, carefully crafted to slay your enemies",
            price : 31,
            quantity : 1,
            image: "longsword.png"
        }
    ];

    function simulateSuccessfulRequest(result) {
        let deferred = $.Deferred();

        setTimeout(
            function() {
                deferred.resolve(result);
            }, 
            Math.random() * 100
        );

        return deferred.promise();
    }

    function simulateFailureRequest() {
        let deferred = $.Deferred();

        setTimeout(
            function() {
                deferred.reject();
            }, 
            Math.random() * 100
        );

        return deferred.promise();
    }

    return {
        getUser: function() {
            return simulateSuccessfulRequest(user);
        },
        list: function() {
            return simulateSuccessfulRequest(items);
        }
    };

})();